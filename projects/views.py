from django.shortcuts import render, get_object_or_404, redirect
from projects.models import Project
from django.contrib.auth.decorators import login_required
from projects.forms import CreateForm

# Create your views here.


@login_required
def list_projects(request):
    allproj = Project.objects.filter(owner=request.user)
    context = {"list": allproj}
    return render(request, "projects/listproj.html", context)


@login_required
def show_project(request, id):
    each = get_object_or_404(Project, id=id)
    context = {"project": each}
    return render(request, "projects/detail.html", context)


@login_required
def create_project(request):
    if request.method == "POST":
        created = CreateForm(request.POST)
        if created.is_valid():
            project = created.save(False)
            project.owner = request.user
            created.save()
            return redirect("list_projects")
    else:
        created = CreateForm()
    context = {"form": created}
    return render(request, "projects/create.html", context)
