from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from tasks.forms import TaskForm
from tasks.models import Task

# Create your views here.


@login_required
def create_task(request):
    if request.method == "POST":
        taskform = TaskForm(request.POST)
        if taskform.is_valid():
            taskform.save()
            return redirect("list_projects")
    else:
        taskform = TaskForm()
    context = {
        "form": taskform,
    }
    return render(request, "tasks/create.html", context)


@login_required
def show_my_tasks(request):
    tasks = Task.objects.filter(assignee=request.user)
    context = {"task_list": tasks}
    return render(request, "tasks/mytasks.html", context)
